# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 11:41:51 2019

@author: TheMathCompany
"""
import pandas as pd
import numpy as np
@pd.api.extensions.register_dataframe_accessor("evaluation")
class evaluation(object):
    
    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def mape(self,model_fit,depVar):
        """
        This Function retruns the MAPE of model given.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of holt's winter
        
        depVar           : column name of the dependent variable as string
        
        """
        df = self._obj
        residuals = model_fit.resid
        print(residuals)
        mape = np.mean(np.abs((residuals)/df[depVar])) * 100
        print("Train MAPE : %f"%mape)
        return(mape)    

    def aic(self,model_fit):
        """
        This Function retruns the AIC of model given.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of holt's winter
                
        """
        aic = model_fit.aic
        print("AIC : ",aic)
        return(aic)    
      
    def residuals(self,model_fit):
        print("Residuals description")
        print("---------------------")
        resids = model_fit.resid
        residuals = resids.describe()
        print(residuals)
        return(residuals)