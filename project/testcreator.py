# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 16:25:21 2019

@author: Akhil Saraswat
"""
import pandas as pd
from timeSeriesPlots import timeSeriesPlot as tsplot
import sarima
import UCM
import holtWinter
import expSmoothing
from plotly.offline import plot
import pickle


df = pd.read_csv("unemp_rate.csv")
df.date = pd.to_datetime(df.date)
df.index = df.date
df.drop(['date'],axis=1,inplace=True)
df.tsplot.plotAcfPacf('value',nlags=100)

trend,seasonality,irregularity = df.tsplot.stl('value')
config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
plot(trend,config=config)
plot(seasonality,config=config)
plot(irregularity,config=config)

train = pd.DataFrame(df[:int(len(df)*7/10)])
test = pd.DataFrame(df[int(len(df)*7/10):])
train
#df_nos = pd.DataFrame([[1,2,3,4], [4,4,4,4], [5,6,7,8],[13,25,61,1],[34,23,12,10]])
#df_mixed = pd.DataFrame([[1,2,3,4], [3,4,5,6], ['x',6,7,8]])
#
#clusterdf,model = df_nos.kmeans.build(n_clusters=3, random_state=3)
#clusterdf.to_csv("clusterdf.csv",index=False)
#pickleFile = open("kmeans.txt", 'wb')
#pickle.dump(model, pickleFile)
#pickleFile.close()

build_model =train.sarima.build(depVar='value', order = (4,0,1), seasonal_order = (6,0,5,3))
model_fit = build_model[0]
parameters = build_model[1]
parameters
pickleFile = open("sarima", 'wb')
pickle.dump(model_fit,pickleFile)
pickleFile.close()
dfWithTrainPred=tsf.TrainPred(train,model_fit,depVar='value')
dfWithTestPred=tsf.TestPred(test,model_fit,depVar='value')
summary = open("summarySarima.txt", 'w')
summary.write(str(df.sarima.getSummary(model_fit,'value')))
summary.close()
train = pd.DataFrame(self.df[:int(len(self.df)*7/10)])
test = pd.DataFrame(self.df[int(len(self.df)*7/10):])
train.sarima.scoreTrain(model_fit,'value').to_csv("sarimaTrainPred.csv")
test.sarima.scoreTest(model_fit,'value').to_csv("sarimaTestPred.csv")
test.dropna(axis=1,inplace=True)
test.arima.scoreTest(model_fit,'value')
model_fit.forecast(len(test))[0]



build_model =train.ucm.build(depVar='value',seasonal=6,autoregressive=2,irregular=True)
model_fit = build_model[0]
pickleFile = open("UCM", 'wb')
pickle.dump(model_fit, pickleFile)
pickleFile.close()
summary = open("summaryUCM.txt", 'w')
summary.write(str(df.ucm.getSummary(model_fit,'value')))
summary.close()
train.ucm.scoreTrain(model_fit,'value').to_csv("UCMTrainPred.csv")
test.ucm.scoreTest(model_fit,'value').to_csv("UCMTestPred.csv")
train.ucm.scoreTrain(model_fit,'value')
model_fit

build_model =train.holtwinter.build('value',dates=train.index, seasonal_periods=15, seasonal='add')
model_fit = build_model
pickleFile = open("hw", 'wb')
pickle.dump(model_fit, pickleFile)
pickleFile.close()
summary = open("summaryhw.txt", 'w')
summary.write(str(df.holtwinter.getSummary(model_fit,'value')))
summary.close()
train.holtwinter.scoreTrain(model_fit,'value').to_csv("hwTrainPred.csv")
test.holtwinter.scoreTest(model_fit,'value').to_csv("hwTestPred.csv")
train.holtwinter.scoreTrain(model_fit,'value')
model_fit.aic
df.holtwinter.getSummary(model_fit,'value')

build_model =train.expsmoothing.build(depVar='value', optimized=True)
model_fit = build_model
pickleFile = open("exp", 'wb')
pickle.dump(model_fit, pickleFile)
pickleFile.close()
train.expsmoothing.scoreTrain(model_fit,'value').to_csv("expTrainPred.csv")
test.expsmoothing.scoreTest(model_fit,'value').to_csv("expTestPred.csv")
train.holtwinter.scoreTrain(model_fit,'value')
model_fit.aic
#df.holtwinter.getSummary(model_fit,'value')